package com.llc.port;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 	当要运行在tomcat等三方插件是，要实现SpringBootServletInitializer，不然认不到项目
 * @author linlvcao
 * @email 1013573731@qq.com
 * @time 2019年12月16日 下午4:43:58
 *
 */
@SpringBootApplication		//	标注程序为Spring-boot项目
@EnableScheduling			//	开启定时功能
public class PortApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(PortApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PortApplication.class);
	}

}
