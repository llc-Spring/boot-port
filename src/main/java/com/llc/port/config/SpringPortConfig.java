package com.llc.port.config;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 *	这个配置文件仅在直接通过PortApplication.java运行启动才会生效
 * @author linlvcao
 * @email 1013573731@qq.com
 * @time 2019年12月16日 下午4:40:06
 *
 */
@Component
public class SpringPortConfig implements ApplicationListener<WebServerInitializedEvent> {

    private int serverPort;

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        this.serverPort = event.getWebServer().getPort();
        System.err.println("init…………");
    }

    public int getPort() {
        return this.serverPort;
    }
}
