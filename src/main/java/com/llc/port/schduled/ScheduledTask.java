package com.llc.port.schduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.llc.port.config.GetPort;
import com.llc.port.config.PortConfig;
import com.llc.port.config.SpringPortConfig;

/**
 * 	获取当前项目运行端口测试----定时
 * @author linlvcao
 * @email 1013573731@qq.com
 * @time 2019年12月16日 下午4:40:29
 *
 */
@Component
public class ScheduledTask {

	/**
	 * 	运行在tomcat环境下
	 */
	@Autowired
	private GetPort tomcatPort;
	
	/**
	 * 	直接启动项目
	 */
	@Autowired
	private SpringPortConfig springPort;
	
	/**
	 * 	上面两个功能整合
	 */
	@Autowired
	private PortConfig portConfig;

	/**
	 * 	每三秒执行一次，便于测试
	 */
	@Scheduled(cron = "0/3 * * * * *")
	public void reportCurrentTimeCron() {
		System.err.println("=======tomcat========");
		System.err.println(tomcatPort.getPort());
		
		
		System.err.println("========spring-boot=======");
		System.err.println(springPort.getPort());
		
		
		System.err.println("============整合===============");
		System.err.println(portConfig.getProPort());
	}
	
}